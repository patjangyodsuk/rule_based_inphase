import nzae
from InPhaseOnline_Netezza import InPhaseOnline_Netezza



class IP_RuleBased_Batch(nzae.Ae):
    
    DateList = [ 0 ]
    PNL = [ 0.0 ]
    InitialEquity = [ 0.0 ]
    MinLength = 0

    def _range(self, start, end, step):
        range_list = []
        cur = start
        while cur < end:
            range_list.append( cur )
            cur += step
        return range_list

    def _runUdtf(self):

        for row in self:
            
            pnldate, pnl, initialequity, min_profit, max_time_between_high, rown, totalrow = row

            if rown == 1:
                self.DateList = []
                self.PNL = []
                self.InitialEquity = []
                self.MinLength = 0


            self.DateList.append( pnldate )
            self.PNL.append( float(pnl) )
            self.InitialEquity.append( float( initialequity ) )

            
            if rown == totalrow:
                MaxDDs = self._range(0.5, min(4.5, min_profit), 0.5)
                MaxInPhaseDDs = range(10, 80, 10)
                for maxdd in MaxDDs:
                    for max_inphase_dd in MaxInPhaseDDs:
                        ip = InPhaseOnline_Netezza(min_profit, maxdd, self.MinLength, max_inphase_dd, max_time_between_high)
                        ip.loadNZData( self.DateList, self.PNL, self.InitialEquity )
                        ip.getPhase()
                        for i in range(0, len(ip.InPhasePNL)):
                            self.output( min_profit, maxdd, max_inphase_dd, max_time_between_high, ip.DateList[i], ip.InPhasePNL[i], self.InitialEquity[i] )

                    
IP_RuleBased_Batch.run()

