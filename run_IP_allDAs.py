import os
import time
import numpy
import pyodbc
from datetime import datetime, timedelta, date
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from matplotlib.backends.backend_pdf import PdfPages



""" INITIALIZE """
isDebug = False
ResultDir = "Results_PROD"
DAVersions = ['DA9.5'] #['DA9.5', 'DA10', 'DA10.5', 'DA11', 'DA12', 'DA12.5']
# DA9.5 - 4, DA10 - 2, DA10.5 - 9, DA11 - 5, DA12 - 6, DA12.5 - 7
MonitoringIDs = [4]
Trains = [4, 8, 12, 16, 24, 52, 104]
# DA9.5, DA10, DA12.5 - 12, DA10.5 - 1, DA11 - 6, DA12 - 2
Retrains =  [12] #[1, 2, 3, 4, 6, 8, 10, 12]
MinProfits = numpy.arange(2, 6.5, 0.5)
MaxInPhaseDDs = numpy.arange(10, 80, 10)
MaxTimeBetweenHighs = numpy.arange(5, 29, 3)
param_file = "COMPLETED_IP_PARAMETERS.CSV"
anchor_file = "COMPLETED_IP_PROCESS_%d.CSV"


######################################################
def uploadDataFromFile(tablename, filename):
    query = """ INSERT INTO ADMIN.%s
                SELECT * FROM 
                EXTERNAL 'C:\\Users\\pjangyodsuk\\Documents\\Projects\\InPhase4Prediction\\InPhase_WalkForward\\NetezzaV2\\%s'
                USING
                ( DELIMITER ','
                  Y2BASE 2000
                  ENCODING 'internal'
                  REMOTESOURCE 'ODBC'
                  ESCAPECHAR '\\' ); """ % (tablename, filename)
    cs.execute( query )
    cs.commit()
    connection.commit()
######################################################



connection = pyodbc.connect('DRIVER={NetezzaSQL};SERVER=192.168.1.153;DATABASE=RESEARCH;UID=pjangyodsuk;PWD=Iolap3goPJ')
cs = connection.cursor()


isTempTable = "TEMP"
if isDebug:
    isTempTable = ""



### STEP 1: PREPARE PARAMETERS ###
start_time = time.time()
""" CREATE TEMP PARAMETERS TABLE """
query = """ CREATE %s TABLE IF NOT Exists ADMIN.IP_PARAMETERS
            ( PID INTEGER,
              TRAIN INTEGER,
              MIN_PROFIT DOUBLE PRECISION,
              MAX_DD DOUBLE PRECISION,
              MAX_INPHASE_DD INTEGER,
              MAX_TIME_BETWEEN_HIGH INTEGER ) DISTRIBUTE ON RANDOM; """ % (isTempTable)
cs.execute( query )
cs.commit()
connection.commit()

if not os.path.exists(param_file):
    pid = 1
    for Train in Trains:
        for MinProfit in MinProfits:
            MaxDDs = numpy.arange(0.5, min(4.5, MinProfit), 0.5)
            for MaxDD in MaxDDs:
                for MaxInPhaseDD in MaxInPhaseDDs:
                    for MaxTimeBetweenHigh in MaxTimeBetweenHighs:
                        query = """ INSERT INTO ADMIN.IP_PARAMETERS
                                    VALUES( %d, %d, %f, %f, %d, %d ) """ \
                                    % (pid, Train, MinProfit, MaxDD, MaxInPhaseDD, MaxTimeBetweenHigh)
                        cs.execute( query )
                        cs.commit()
                        connection.commit()
                        pid += 1
else:
    uploadDataFromFile('IP_PARAMETERS', param_file)
    
print "Finish TEMP TABLE: IP_PARAMETERS. (Elapse time %f s)" % (time.time() - start_time)


### STEP 2: PREPARE ALL TEMP TABLES ###
""" CREATE TEMP PROCESSING ANCHOR TABLE """
query = """ CREATE %s TABLE IF NOT Exists ADMIN.IP_PROCESS
            ( PROCESSID INTEGER,
              TRAIN INTEGER,
              STARTTRAIN DATE,
              ENDTRAIN DATE,
              STARTTEST DATE,
              ENDTEST DATE) DISTRIBUTE ON RANDOM; """ % (isTempTable)
cs.execute( query )
cs.commit()
connection.commit()


""" CREATE TEMP INTERMEDIATE PNL TABLE """
query = """ CREATE %s TABLE IF NOT Exists ADMIN.IP_INTERMEDIATE_PNL
            ( MIN_PROFIT DOUBLE PRECISION,
              MAX_DD DOUBLE PRECISION,
              MAX_INPHASE_DD INTEGER,
              MAX_TIME_BETWEEN_HIGH INTEGER,
              RPTDAY DATE,
              PNL DOUBLE PRECISION,
              INITIALEQUITY DOUBLE PRECISION) DISTRIBUTE ON RANDOM; """ % (isTempTable)
cs.execute( query )
cs.commit()
connection.commit()


query = """ CREATE %s TABLE IF NOT Exists ADMIN.IP_EVALUATION
            ( PID INTEGER,
              PROCESSID INTEGER,
              RR_OVER_MAXDD DOUBLE PRECISION ) DISTRIBUTE ON RANDOM; """  % (isTempTable)
cs.execute( query )
cs.commit()
connection.commit()


for Retrain in Retrains:

    ### STEP 3: PREPARE PROCESS FOR CURRENT RETRAIN ###
    start_time = time.time()
    cs.execute( "TRUNCATE TABLE ADMIN.IP_PROCESS;" )
    cs.commit()
    connection.commit()

    if not os.path.exists(anchor_file):
        print "CREATE TABLE IP_PROCESS ... "
        """ READ FIRST DAY FROM TS DATA """
        query = """ SELECT MIN(RPTDAY), MAX(RPTDAY)
                    FROM ADMIN.MONITORINGDAILYPNL; """
        cs.execute( query )
        result = cs.fetchone()
        first_possible_testdate = result[0] + timedelta(weeks=max(Trains))
        lastdate = result[1] #date(2015, 12, 31)

        processid = 1
        for Train in Trains:
            start_traindate = first_possible_testdate - timedelta(weeks=Train)
            start_testdate = first_possible_testdate
            end_traindate = first_possible_testdate - timedelta(days=1)
            end_testdate = first_possible_testdate + timedelta(weeks=Retrain) - timedelta(days=1)

            while start_testdate <= lastdate:
                query = """ INSERT INTO ADMIN.IP_PROCESS
                            VALUES( %d, %d, '%s', '%s', '%s', '%s' ); """ % (processid, Train, start_traindate, end_traindate, \
                                                                             start_testdate, end_testdate)
                cs.execute( query )
                cs.commit()
                connection.commit()
                start_traindate += timedelta(weeks=Retrain)
                start_testdate += timedelta(weeks=Retrain)
                end_traindate += timedelta(weeks=Retrain)
                end_testdate += timedelta(weeks=Retrain)
                processid += 1
    else:
        uploadDataFromFile('IP_PARAMETERS', anchor_file % (Retrain))
        
    print "Finish generating IP_PROCESS for RETRAIN %d. (Elapse time %f s)" % (Retrain, time.time() - start_time)
    
        
    for d in range(len(DAVersions)):

        DAVersion = DAVersions[d]

        print "++++++++++++++++ PROCESSING %s  ++++++++++++++++" % (DAVersion, )


        ### STEP 4: RUN INPHASE FOR ALL PARAMETERS ###
        start_time = time.time()
        cs.execute( "TRUNCATE TABLE ADMIN.IP_INTERMEDIATE_PNL;" )
        cs.commit()
        connection.commit()

        """ GENERATE INPHASE PNL FOR ALL PARAMETER """
        query = """ INSERT INTO ADMIN.IP_INTERMEDIATE_PNL
                    SELECT output.MIN_PROFIT, output.MAX_DD, output.MAX_INPHASE_DD, output.MAX_TIME_BETWEEN_HIGH, '1970-01-01'::date + output.TESTDATE * interval '1 second' As RPTDAY, output.INPHASEPNL, output.INITIALEQUITY	  
                    FROM (
                            SELECT PID, MIN_PROFIT, MAX_TIME_BETWEEN_HIGH, RPTDAY, extract(epoch from RPTDAY) as LONGRPTDAY, PNL, INITIALEQUITY, row_number() over (partition by PID order by RPTDAY) as rown, count(*) over (partition by PID) as totalrow
                             FROM 
                                   (SELECT RPTDAY, PNL
                                   FROM ADMIN.MONITORINGDAILYPNL
                                   WHERE MONITORINGID = %d ) AS pnl
                             JOIN (SELECT PRDATE, SUM(INITIALEQUITY) AS INITIALEQUITY
                                   FROM (
                                        SELECT p2.MARKETID, CAST(OPENPRICEDATE AS DATE) AS PRDATE, MARKETOPEN*m.BIGPOINTVALUE AS INITIALEQUITY
                                        FROM ADMIN.PRICES AS p2
                                        JOIN( SELECT MARKETID, MIN(PRICEDATE) AS OPENPRICEDATE
                                              FROM ADMIN.PRICES
                                              WHERE MARKETID <= 14 AND MARKETID <> 11
                                              GROUP BY MARKETID, CAST(PRICEDATE AS DATE) ) AS p1
                                        ON p1.MARKETID = p2.MARKETID AND p1.OPENPRICEDATE = p2.PRICEDATE
                                        JOIN ADMIN.MARKETS AS m
                                        ON p2.MARKETID = m.MARKETID) AS tmp
                                   GROUP BY PRDATE) AS ie
                               ON RPTDAY = PRDATE
                             CROSS JOIN (
                                   SELECT MIN(PID) AS PID, MIN_PROFIT, MAX_TIME_BETWEEN_HIGH
                                   FROM ADMIN.IP_PARAMETERS
                                   GROUP BY MIN_PROFIT, MAX_TIME_BETWEEN_HIGH) AS param ) AS input,
                    TABLE WITH FINAL
                            ( IP_RuleBased_Batch(LONGRPTDAY, CAST(PNL AS DOUBLE), CAST(INITIALEQUITY AS DOUBLE), CAST(MIN_PROFIT AS DOUBLE), MAX_TIME_BETWEEN_HIGH, rown, totalrow) ) AS output; """ % (MonitoringIDs[d])
        cs.execute( query )
        cs.commit()
        connection.commit()

        print "Finish getting InPhase PNL for all parameters. (Elapse time %f s)" % (time.time() - start_time)



        ### STEP 5: EVALUATION ###
        start_time = time.time()
        cs.execute( "TRUNCATE TABLE ADMIN.IP_EVALUATION;" )
        cs.commit()
        connection.commit()

        query = """ INSERT INTO ADMIN.IP_EVALUATION
                    SELECT output.*
                    FROM (   SELECT PID, PROCESSID, extract(epoch from RPTDAY) as LONGRPTDAY, PNL, INITIALEQUITY, row_number() over (partition by PID, PROCESSID order by RPTDAY) as rown, count(*) over (partition by PID, PROCESSID) as totalrow
                             FROM (
                                 SELECT PID, PROCESSID, RPTDAY, PNL, INITIALEQUITY
                                 FROM ADMIN.IP_PROCESS AS pr
                                 JOIN ADMIN.IP_INTERMEDIATE_PNL AS pnl
                                   ON pnl.RPTDAY >= pr.STARTTRAIN AND pnl.RPTDAY <= pr.ENDTRAIN
                                 JOIN ADMIN.IP_PARAMETERS AS param
                                   ON pnl.MIN_PROFIT = param.MIN_PROFIT AND pnl.MAX_DD = param.MAX_DD AND pnl.MAX_INPHASE_DD = param.MAX_INPHASE_DD AND pnl.MAX_TIME_BETWEEN_HIGH = param.MAX_TIME_BETWEEN_HIGH AND pr.TRAIN = param.TRAIN
                                  ) AS tmp1
                             ORDER BY PID, PROCESSID, RPTDAY   ) AS input,
                    TABLE WITH FINAL
                        ( IP_GetEvaluation(PID, PROCESSID, LONGRPTDAY, PNL, INITIALEQUITY, rown, totalrow) ) AS output; """
        cs.execute( query )
        cs.commit()
        connection.commit()
            
        print "Finish Evaluation. (Elapse time %f s)" % (time.time() - start_time)


        ### STEP 6: GET SELECTED PARAMETERS ###
        start_time = time.time()
        query = """  SELECT eb.STARTTEST, eb.ENDTEST, eb.MAX_MARP, param.TRAIN, param.MIN_PROFIT, param.MAX_DD, param.MAX_INPHASE_DD, param.MAX_TIME_BETWEEN_HIGH
                     FROM (
                             SELECT STARTTEST, ENDTEST, MAX(RR_OVER_MAXDD) AS MAX_MARP
                             FROM ADMIN.IP_EVALUATION AS e1
                             JOIN ADMIN.IP_PROCESS AS p1
                                ON e1.PROCESSID = p1.PROCESSID
                             GROUP BY STARTTEST, ENDTEST
                     ) AS eb
                     JOIN (
                             SELECT p2.STARTTEST, p2.ENDTEST, MIN(e2.PID) AS PID, e2.RR_OVER_MAXDD
                             FROM ADMIN.IP_EVALUATION AS e2 
                             JOIN ADMIN.IP_PROCESS AS p2
                                ON e2.PROCESSID = p2.PROCESSID
                             GROUP BY p2.STARTTEST, p2.ENDTEST, e2.RR_OVER_MAXDD
                     ) AS pf
                     ON eb.STARTTEST = pf.STARTTEST AND eb.ENDTEST = pf.ENDTEST AND eb.MAX_MARP = pf.RR_OVER_MAXDD
                     JOIN ADMIN.IP_PARAMETERS AS param
                     ON pf.PID = param.PID
                     ORDER BY eb.STARTTEST; """
        cs.execute(query)
        params = cs.fetchall()

        with open("params.csv", "wb") as ofile:
            for param in params:
                ofile.write("%s,%s,%f,%d,%f,%f,%d,%d\n" % (param[0].strftime("%Y-%m-%d"), param[1].strftime("%Y-%m-%d"), param[2], param[3], param[4], param[5], param[6], param[7]))

        print "Finish Retrieving Walk Forward Selected Parameters. (Elapse time %f s)" % (time.time() - start_time)


        ### STEP 6: RETRIEVE INPHASE PNL ###
        start_time = time.time()
        query = """  SELECT pnl.RPTDAY, pnl.PNL, pnl.INITIALEQUITY
                     FROM (
                             SELECT STARTTEST, ENDTEST, MAX(RR_OVER_MAXDD) AS MAX_MARP
                             FROM ADMIN.IP_EVALUATION AS e1
                             JOIN ADMIN.IP_PROCESS AS p1
                                ON e1.PROCESSID = p1.PROCESSID
                             GROUP BY STARTTEST, ENDTEST
                     ) AS eb
                     JOIN (
                             SELECT p2.STARTTEST, p2.ENDTEST, MIN(e2.PID) AS PID, e2.RR_OVER_MAXDD
                             FROM ADMIN.IP_EVALUATION AS e2 
                             JOIN ADMIN.IP_PROCESS AS p2
                                ON e2.PROCESSID = p2.PROCESSID
                             GROUP BY p2.STARTTEST, p2.ENDTEST, e2.RR_OVER_MAXDD
                     ) AS pf
                     ON eb.STARTTEST = pf.STARTTEST AND eb.ENDTEST = pf.ENDTEST AND eb.MAX_MARP = pf.RR_OVER_MAXDD
                     JOIN ADMIN.IP_PARAMETERS AS param
                     ON pf.PID = param.PID
                     JOIN ADMIN.IP_INTERMEDIATE_PNL AS pnl
                     ON pnl.RPTDAY >= eb.STARTTEST AND pnl.RPTDAY <= eb.ENDTEST AND pnl.MIN_PROFIT = param.MIN_PROFIT AND pnl.MAX_DD = param.MAX_DD AND pnl.MAX_INPHASE_DD = param.MAX_INPHASE_DD AND pnl.MAX_TIME_BETWEEN_HIGH = param.MAX_TIME_BETWEEN_HIGH
                     ORDER BY RPTDAY; """
        cs.execute( query )
        inphase_pnl = cs.fetchall()

        print "Finish Retrieving Walk Forward InPhase PNL. (Elapse time %f s)" % (time.time() - start_time)



        ### STEP 7: GENERATE OUTPUT CSV AND PDF ###
        start_time = time.time()
        DateList = [ '' ]
        IPEquity = [ float(inphase_pnl[0][2]) ]
        for record in inphase_pnl:
            DateList.append( record[0] )
            IPEquity.append( IPEquity[-1] + float(record[1]) )

        query = """ SELECT RPTDAY, PNL
                    FROM ADMIN.MONITORINGDAILYPNL
                    WHERE MONITORINGID = %d AND RPTDAY >= '%s'
                    ORDER BY RPTDAY; """ % (MonitoringIDs[d], inphase_pnl[0][0].strftime("%Y-%m-%d")) # AND RPTDAY <= '2015-12-31'
        cs.execute( query )
        da_pnl = cs.fetchall()

        DAEquity = [ float(inphase_pnl[0][2]) ]
        for record in da_pnl:
            DAEquity.append( DAEquity[-1] + float(record[1]) )

        NSep = len(DateList) / 10
        dateplot = [ ]
        for i in range(0, len(DateList), NSep):
            dateplot.append( DateList[i] )

            
        """ PLOT TO PDF """
        if not os.path.exists("./%s/PDF/%s/" % (ResultDir, DAVersion)):
            os.makedirs("./%s/PDF/%s/" % (ResultDir, DAVersion))

        with PdfPages("./%s/PDF/%s/InPhaseWF_%s_%d.pdf" % (ResultDir, DAVersion, DAVersion, Retrain)) as pdf:
            plt.rcParams.update({'font.size': 8})
            fig = plt.figure()
            ax = fig.add_axes((.1,.3,.8,.6))
            """ PLOT Equity """
            plt.plot(range(0, len(DAEquity)), DAEquity, 'k', linewidth=0.2)
            plt.hold(True)
            """ PLOT NEW EQUITY """
            plt.plot(range(0, len(IPEquity)), IPEquity, 'r', linewidth=0.2)
            ax.ticklabel_format(useOffset=False)
            plt.title("In-Phase Detection: %s Retrain %d weeks" % (DAVersion, Retrain))
            plt.xlabel('Date')
            plt.ylabel('Equity ($)')
            plt.xticks(range(0, len(DateList), NSep), dateplot, rotation='vertical')
            plt.hold(False)
            pdf.savefig(fig)
            plt.close()

        
        """ SAVE RESULT TO CSV """
        if not os.path.exists("./%s/CSV/%s/" % (ResultDir, DAVersion)):
            os.makedirs("./%s/CSV/%s/" % (ResultDir, DAVersion))
            
        with open("./%s/CSV/%s/InPhaseWF_%s_%d.csv" % (ResultDir, DAVersion, DAVersion, Retrain), "wb") as ofile:
            ofile.write("DATE,DA_Equity,IP_Equity\n")
            for i in range(0, len(DAEquity)):
                ofile.write("%s,%f,%f\n" % (DateList[i], DAEquity[i], IPEquity[i]))
                
        print "Finish generating outputs. (Elapse time %f s)" % (time.time() - start_time)


cs.close()
del cs
connection.close()
