import csv


class InPhaseOnline_Netezza:

    def __init__(self, MinProfit, MaxDD, MinLength, MaxInPhaseDD, MaxTimeBetweenHigh):
        self.DateList = []
        self.PNL = []
        self.initialEquity = []
        self.Phases = []
        self.Tracking = []
        self.inPhaseEquity = []
        self.InPhasePNL = []
        self.InPhaseDateList = []
        self.param_MinProfit = float(MinProfit)/100
        self.param_MaxDD = float(MaxDD)/100
        self.param_MinLength = MinLength
        self.param_MaxInPhaseDD = float(MaxInPhaseDD)/100
        self.param_MaxTimeBetweenHigh = MaxTimeBetweenHigh
            

    def loadNZData(self, datelist, pnl, initialequity):
        self.DateList = []
        self.PNL = []        
        self.initialEquity = []
        self.inPhaseEquity = [ initialequity[0] ]
        for i in range(0, len(datelist)):
            self.DateList.append( datelist[i] )
            self.PNL.append( pnl[i] )
            self.initialEquity.append( initialequity[i] )


    def resetVariable(self, index):
        if index < len(self.PNL):
            anchorindex = index
            current_phase_equity = self.initialEquity[index]
            lowest_equity_since_anchor = self.initialEquity[index]
            highest_equity_since_anchor = self.initialEquity[index]
            highest_equity_since_anchor_index = index
            max_drawdown_since_anchor = 0.0
            max_profit_since_anchor = 0.0
            self.current_initialEquity = self.initialEquity[index]
            self.Tracking.append( [ self.DateList[index], current_phase_equity, 0.0, 0.0, 0, 0.0, 0.0, 0 ] )
            return [anchorindex, current_phase_equity, lowest_equity_since_anchor, \
                    highest_equity_since_anchor, highest_equity_since_anchor_index, max_drawdown_since_anchor, max_profit_since_anchor]
        else:
            return [0, 0, 0, 0, 0, 0, 0]


    def getPhase(self):
        phase_number = 1
        isInPhase = False
        [anchorindex, current_phase_equity, lowest_equity_since_anchor, highest_equity_since_anchor, \
         highest_equity_since_anchor_index, max_drawdown_since_anchor, max_profit_since_anchor] = self.resetVariable(0)
        

        i = 0
        while i < len(self.PNL):

            current_phase_equity += self.PNL[i]
            
##            """ ADJUST ANCHOR EQUITY WHEN EQUITY IS LOWER THAN PREV ANCHOR """
##            if not isInPhase and current_phase_equity < self.current_initialEquity:
##                self.Tracking.append( [ self.DateList[i], current_phase_equity, max_drawdown_since_anchor, max_profit_since_anchor, i-highest_equity_since_anchor_index, \
##                                    highest_equity_since_anchor, lowest_equity_since_anchor, phaseIndicator ] )
##                [anchorindex, current_phase_equity, lowest_equity_since_anchor, highest_equity_since_anchor, \
##                 highest_equity_since_anchor_index, max_drawdown_since_anchor, max_profit_since_anchor] = self.resetVariable(i+1)
##                i += 1
##                self.inPhaseEquity.append( self.inPhaseEquity[-1] )
##                self.InPhasePNL.append( 0.0 )
##                continue


            ############# TRACKING INFO SINCE ANCHOR ############
            """ ADJUST HIGHEST HIGH SINCE ANCHOR """
            if current_phase_equity > highest_equity_since_anchor:
                highest_equity_since_anchor = current_phase_equity
                highest_equity_since_anchor_index = i
                max_drawdown_since_anchor = 0.0

            """ TRCK MAX PROFIT SINCE ANCHOR """
            if self.PNL[i] > 0:
                max_profit_since_anchor += self.PNL[i]

            """ TRACK MAX DRAWDOWN SINCE ANCHOR """
            current_drawdown = highest_equity_since_anchor - current_phase_equity
            if current_drawdown > max_drawdown_since_anchor:
                max_drawdown_since_anchor = current_drawdown
                lowest_equity_since_anchor = current_phase_equity
            elif current_drawdown == 0:
                lowest_equity_since_anchor = current_phase_equity

            phaseIndicator = 0
            if isInPhase:
                phaseIndicator = phase_number
            self.Tracking.append( [ self.DateList[i], current_phase_equity, max_drawdown_since_anchor, max_profit_since_anchor, i-highest_equity_since_anchor_index, \
                                    highest_equity_since_anchor, lowest_equity_since_anchor, phaseIndicator ] )
            ############# TRACKING INFO SINCE ANCHOR ############
                

            ############# SWITCHING BETWEEN IN AND OUT OF PHASE #############     
            if not isInPhase:
                
               self.inPhaseEquity.append( self.inPhaseEquity[-1] )
               self.InPhasePNL.append( 0.0 )
               self.InPhaseDateList.append( self.DateList[i] )

               if current_phase_equity < self.current_initialEquity:
                   [anchorindex, current_phase_equity, lowest_equity_since_anchor, highest_equity_since_anchor, \
                    highest_equity_since_anchor_index, max_drawdown_since_anchor, max_profit_since_anchor] = self.resetVariable(i+1)
                
                
               elif (highest_equity_since_anchor - self.current_initialEquity)/self.current_initialEquity > self.param_MinProfit \
                   and (highest_equity_since_anchor - lowest_equity_since_anchor)/ lowest_equity_since_anchor < self.param_MaxDD \
                   and (i - anchorindex + 1) >= self.param_MinLength:
                    phase_start_index = i
                    isInPhase = True
                   
               elif (highest_equity_since_anchor - lowest_equity_since_anchor)/ lowest_equity_since_anchor >= self.param_MaxDD:
                    [anchorindex, current_phase_equity, lowest_equity_since_anchor, highest_equity_since_anchor, \
                     highest_equity_since_anchor_index, max_drawdown_since_anchor, max_profit_since_anchor] = self.resetVariable(i+1)
                    
                        
            else:

                self.inPhaseEquity.append( self.inPhaseEquity[-1] + self.PNL[i] )
                self.InPhasePNL.append( self.PNL[i] )
                self.InPhaseDateList.append( self.DateList[i] )
                
                if max_drawdown_since_anchor > (max_profit_since_anchor * self.param_MaxInPhaseDD) \
                   or (i - highest_equity_since_anchor_index) > self.param_MaxTimeBetweenHigh:
                    self.Phases.append( [ phase_start_index, i+1 ] )
                    phase_number += 1
                    isInPhase = False
                    [anchorindex, current_phase_equity, lowest_equity_since_anchor, highest_equity_since_anchor, \
                     highest_equity_since_anchor_index, max_drawdown_since_anchor, max_profit_since_anchor] = self.resetVariable(i+1)

            i += 1
            ############# SWITCHING BETWEEN IN AND OUT OF PHASE ############# 

        if isInPhase:
            self.Phases.append( [ phase_start_index, i ] )
            

    def getMARp(self):
        maxdd = -float('inf')
        highest = -float('inf')
        for val in self.inPhaseEquity:
            if val > highest:
                highest = val
            curdd = (highest - val)
            if curdd > maxdd:
                maxdd = curdd
                
        if maxdd != 0:
            return (self.inPhaseEquity[-1] - self.inPhaseEquity[0]) / maxdd
        else:
            return -float('inf')


    def getRR(self):
        return self.inPhaseEquity[-1] - self.inPhaseEquity[0]


    def getInPhaseLength(self):
        return len(self.inPhaseEquity) - 1


    def getInPhaseTimeRatio(self):
        return float(self.getInPhaseLength()) / len(self.PNL)


    
