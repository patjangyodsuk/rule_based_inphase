import nzae
from InPhaseOnline_Netezza import InPhaseOnline_Netezza



class IP_GetEvaluation(nzae.Ae):

    Equity = [ 0.0 ]

    def _runUdtf(self):

        for row in self:
            
            pid, processid, pnldate, pnl, initialequity, rown, totalrow = row

            if rown == 1:
                self.Equity = [ float( initialequity ) ]
                
            self.Equity.append( self.Equity[-1] + float(pnl) )

            
            if rown == totalrow:
                maxdd = -float('inf')
                highest = -float('inf')
                for val in self.Equity:
                    if val > highest:
                        highest = val
                    curdd = (highest - val)
                    if curdd > maxdd:
                        maxdd = curdd
                        
                if maxdd != 0:
                    self.output( pid, processid, (self.Equity[-1] - self.Equity[0]) / maxdd)
                else:
                    self.output( pid, processid, -float('inf'))


IP_GetEvaluation.run()
