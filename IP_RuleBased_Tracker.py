import nzae
from InPhaseOnline_Netezza import InPhaseOnline_Netezza



class IP_RuleBased_Tracker(nzae.Ae):
    
    DateList = [ 0 ]
    PNL = [ 0.0 ]
    InitialEquity = [ 0.0 ]
    MinLength = 0

    def _runUdtf(self):

        for row in self:
            
            pid, pnldate, pnl, initialequity, min_profit, maxdd, max_inphase_dd, max_time_between_high, rown, totalrow = row

            if rown == 1:
                self.DateList = []
                self.PNL = []
                self.InitialEquity = []
                self.MinLength = 0


            self.DateList.append( pnldate )
            self.PNL.append( float(pnl) )
            self.InitialEquity.append( float( initialequity ) )

            
            if rown == totalrow:
                ip = InPhaseOnline_Netezza(min_profit, maxdd, self.MinLength, max_inphase_dd, max_time_between_high)
                ip.loadNZData( self.DateList, self.PNL, self.InitialEquity )
                ip.getPhase()
                for i in range(0, len(ip.Tracking)):
                    self.output( ip.Tracking[i][0], ip.Tracking[i][1], ip.Tracking[i][2], ip.Tracking[i][3], \
                                 ip.Tracking[i][4], ip.Tracking[i][5], ip.Tracking[i][6], ip.Tracking[i][7])

                    
IP_RuleBased_Tracker.run()

